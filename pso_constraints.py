#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 13:06:05 2024

@author: kamilfatkhiev
"""
import numpy as np
from typing import Callable, Tuple, List

# np.random.seed(100)

# Тестовые функции
def test(x, y):
    return 20 + x**2 + y**2 - 10 * (np.cos(2 * np.pi * x) + np.cos(2 * np.pi * y))
    # return x**2 + y**2
    # return -20 * np.exp(-0.2 * np.sqrt(0.5 * (x**2 + y**2))) - np.exp(0.5 * (np.cos(2 * np.pi * x) + np.cos(2 * np.pi * y))) + np.exp(1) + 20
    # return (1.5 - x + x * y)**2 + (2.25 - x + x * y**2)**2 + (2.625 - x + x * y**3)**2
    # return -np.abs(np.sin(x) * np.cos(y) * np.exp(np.abs(1 - np.sqrt(x**2 + y**2) / np.pi))) #Табличная функция Хольдера (не сходится)
    # return -(y + 47) * np.sin(np.sqrt(np.abs(x / 2 + y + 47))) - x * np.sin(np.sqrt(np.abs(x - y + 47))) # Eggholder (тоже не сходится)
    # return np.sin(x + y) + (x - y)**2 - 1.5 * x + 2.5 * y + 1
    # return 100 * np.sqrt(np.abs(y - 0.01 * x**2)) + 0.01 * np.abs(x + 10)


class PSO():
    
    def __init__(self, 
                 func: Callable,
                 n_variables: int,
                 max_iterations: int = 1000,
                 n_particles: int = 10,
                 w: float = 0.7928,
                 c_1: float = 1.49618,
                 c_2: float = 1.49618,
                 eps: float = 1e-9,
                 boundries: List[Tuple[float, float]] = None):
        self.func = func
        self.max_iterations = max_iterations
        self.n_variables = n_variables
        self.n_particles = n_particles
        self.w = w
        self.c_1 = c_1
        self.c_2 = c_2
        self.eps = eps
        self.boundries = boundries
    
    
    def update_position_and_velocity(self, 
                                     particles: np.ndarray, 
                                     velocities: np.ndarray, 
                                     pbest: np.ndarray, 
                                     gbest: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        
        r_1 = np.random.rand(particles.shape[0], particles.shape[1])
        r_2 = np.random.rand(particles.shape[0], particles.shape[1])
        social_component = self.c_1 * r_1 * (pbest - particles)
        cognitive_component = self.c_2 * r_2 * (gbest[:, None] - particles)
        velocities = self.w * velocities + social_component + cognitive_component
        particles = particles + velocities
        
        if self.boundries:
            for i in range(len(self.boundries)):
                if self.boundries[i]:
                    low, up = self.boundries[i]
                    for j in range(self.n_particles):
                        if particles[i][j] < low:
                            particles[i][j] = low
                        if particles[i][j] > up:
                            particles[i][j] = up
                        else:
                            continue
                else:
                    continue
        
        return particles, velocities
    
    
    def create_population_and_velocity(self) -> Tuple[np.ndarray, np.ndarray]:
        
        if self.boundries:
            particles = np.empty((self.n_variables, self.n_particles))
            velocities = np.empty((self.n_variables, self.n_particles))
            for i in range(len(self.boundries)):
                if self.boundries[i]:
                    low, up = self.boundries[i]
                    particles[i] = np.random.uniform(low, 
                                          up, 
                                          (self.n_particles,))
                    velocities[i] = np.random.uniform(-np.abs(up - low), 
                                           np.abs(up - low), 
                                           (self.n_particles,))
                else:
                    particles[i] = np.random.uniform(0, 1, (self.n_particles,))
                    velocities[i] = np.random.uniform(0, 1, (self.n_particles,))
        else: 
            particles = np.random.uniform(size=(self.n_variables, self.n_particles))
            velocities = np.random.uniform(size=(self.n_variables, self.n_particles))
        
        return particles, velocities
   
    
    def optimize(self) -> dict:
        
        i = 0
        no_diff_count = 0
        gbest_obj = np.inf
        gbest = []
        particles, velocities = self.create_population_and_velocity()
        
        while (i < self.max_iterations) and (no_diff_count < 300):
            if i == 0:
                pbest = particles
                pbest_obj = self.func(*particles)
                gbest = pbest[:, pbest_obj.argmin()]
                gbest_obj = pbest_obj.min()
            else:
                particles, velocities = self.update_position_and_velocity(particles, 
                                                                          velocities, 
                                                                          pbest, 
                                                                          gbest)
                obj = self.func(*particles)
                
                pbest_obj = np.array([pbest_obj, obj]).min(axis=0)
                pbest[:, (pbest_obj >= obj)] = particles[:, (pbest_obj >= obj)]
                
                gbest = pbest[:, pbest_obj.argmin()]
                gbest_obj_prev = gbest_obj
                gbest_obj = pbest_obj.min()
                if np.abs(gbest_obj_prev - gbest_obj) < self.eps:
                    no_diff_count += 1
            i += 1
        
        return {'Function opt.': gbest_obj, 'Opt. position': gbest, 'Number of iterations': i}


optimizer = PSO(func=test, n_variables=2, n_particles=30, boundries=[(-5.12, 5.12), (-5.12, 5.12)])
# optimizer = PSO(func=test, n_variables=2, n_particles=30, boundries=[(-5.0, 5.0), (-5.0, 5.0)])
# optimizer = PSO(func=test, n_variables=2, n_particles=30, boundries=[(-4.5, 4.5), (-4.5, 4.5)])
# optimizer = PSO(func=test, n_variables=2, n_particles=50, boundries=[(-10, 10), (-10, 10)])
# optimizer = PSO(func=test, n_variables=2, n_particles=1000, boundries=[(-512.0, 512.0), (-512.0, 512.0)])
# optimizer = PSO(func=test, n_variables=2, n_particles=30, boundries=[(-1.5, 4.0), (-3.0, 4.0)])
# optimizer = PSO(func=test, n_variables=2, n_particles=50, boundries=[(-15, -5), (-3, 3)])
# results = optimizer.optimize()

# for k, v in results.items():
#     print(k, v)

g_best = []
iterations = []

for _ in range(100):
    result = optimizer.optimize()
    g_best.append(result['Function opt.'])
    iterations.append(result['Number of iterations'])
    
print(sum(g_best) / len(g_best))
print(sum(iterations) / len(iterations))
