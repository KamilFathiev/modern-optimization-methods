# Modern optimization methods

## Описание

В данном репозитории представлен ноутбук с итогами выполнения групповой работы по дисциплине "Современные методы оптимизации" и отдельный файл с реализацией метода роя частиц (PSO).

Состав команды:
- https://gitlab.com/KamilFathiev
- https://gitlab.com/ValeriaTikhevich

## Add your files

```
cd existing_repo
git remote add origin https://gitlab.com/KamilFathiev/modern-optimization-methods.git
git branch -M main
git push -uf origin main
```
