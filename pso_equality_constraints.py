#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  4 13:06:05 2024

@author: kamilfatkhiev
"""
import numpy as np
from typing import Callable, Tuple, List

# np.random.seed(100)

    
def free_energy(x1, x2, x3, x4, x5, x6, x7, x8, x9, x10) -> float:
    
    am_of_sub = sum([x1, x2, x3, x4, x5, x6, x7, x8, x9, x10])
    c1, c2, c3, c4, c5, c6, c7, c8, c9, c10 = [-10.021, -21.096, -37.986, -9.846, -28.653, -18.918, -28.032, -14.640, -30.594, -26.111]
    g = x1 * (c1 + np.log(750) + np.log(x1 / am_of_sub)) \
        + x2 * (c2 + np.log(750) + np.log(x2 / am_of_sub)) \
        + x3 * (c3 + np.log(750) + np.log(x3 / am_of_sub)) \
        + x4 * (c4 + np.log(750) + np.log(x4 / am_of_sub)) \
        + x5 * (c5 + np.log(750) + np.log(x5 / am_of_sub)) \
        + x6 * (c6 + np.log(750) + np.log(x6 / am_of_sub)) \
        + x7 * (c7 + np.log(750) + np.log(x7 / am_of_sub)) \
        + x8 * (c8 + np.log(750) + np.log(x8 / am_of_sub)) \
        + x9 * (c9 + np.log(750) + np.log(x9 / am_of_sub)) \
        + x10 * (c10 + np.log(750) + np.log(x10 / am_of_sub)) \
        + np.sum(np.sin(0.1 * np.array([x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]))) \
        + np.sum(0.1 * np.array([x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]))
    return g


class PSO():
    
    def __init__(self, 
                 func: Callable,
                 n_variables: int,
                 max_iterations: int = 1000,
                 n_particles: int = 10,
                 w: float = 0.7928,
                 c_1: float = 1.49618,
                 c_2: float = 1.49618,
                 eps: float = 1e-9,
                 boundries: List[Tuple[float, float]] = None):
        self.func = func
        self.max_iterations = max_iterations
        self.n_variables = n_variables
        self.n_particles = n_particles
        self.w = w
        self.c_1 = c_1
        self.c_2 = c_2
        self.eps = eps
        self.boundries = boundries
    
    
    def create_particle(self):
        
        particle = np.zeros(10)
        
        while not np.all(particle > 0):
            
            x2, x5, x6, x7, x8, x9, x10 = np.random.uniform(size=(7,))
            x1 = -2 * x2  - x6 + 2 * x7 + 2 * x8 + 4 * x9 + x10
            x3 = 1 - x7 - x8 - 2 * x9 - x10
            x4 = 1 - 2 * x5 - x6 - x7
            particle = np.array([x1, x2, x3, x4, x5, x6, x7, x8, x9, x10])
            
        return particle
    
    
    def create_v(self):
        
        v = np.zeros(10)
        x2, x5, x6, x7, x8, x9, x10 = np.random.uniform(size=(7,))    
        x1 = -2 * x2  - x6 + 2 * x7 + 2 * x8 + 4 * x9 + x10
        x3 = -x7 - x8 - 2 * x9 - x10
        x4 = -2 * x5 - x6 - x7
        v = np.array([x1, x2, x3, x4, x5, x6, x7, x8, x9, x10])
            
        return v
    
    
    def update_position_and_velocity(self, 
                                     particles: np.ndarray, 
                                     velocities: np.ndarray, 
                                     pbest: np.ndarray, 
                                     gbest: np.ndarray,
                                     gbest_id: float) -> Tuple[np.ndarray, np.ndarray]:
        
        r_1 = np.random.rand()
        r_2 = np.random.rand()
        social_component = self.c_1 * r_1 * (pbest - particles)
        cognitive_component = self.c_2 * r_2 * (gbest[:, None] - particles)
        velocities = self.w * velocities + social_component + cognitive_component
        particles = particles + velocities
        
        v = self.create_v()
        particles[:, gbest_id] = pbest[:, gbest_id] + v
        
        for i in range(self.n_particles):
            if np.all(particles[:, i] > 0):
                continue
            else:
                particles[:, i] = self.create_particle()

        return particles, velocities
    
    
    def create_population_and_velocity(self) -> Tuple[np.ndarray, np.ndarray]:
        
        particles = np.empty((self.n_variables, self.n_particles))
        velocities = np.zeros((self.n_variables, self.n_particles))
        
        for i in range(self.n_particles):
            particles[:, i] = self.create_particle()
        
        return particles, velocities
   
    
    def optimize(self) -> dict:
        
        i = 0
        no_diff_count = 0
        gbest_obj = np.inf
        gbest = []
        particles, velocities = self.create_population_and_velocity()
        
        while (i < self.max_iterations) and (no_diff_count < 300):
            if i == 0:
                pbest = particles
                pbest_obj = self.func(*particles)
                gbest_id = pbest_obj.argmin()
                gbest = pbest[:, gbest_id]
                gbest_obj = pbest_obj.min()
            else:
                particles, velocities = self.update_position_and_velocity(particles, 
                                                                          velocities, 
                                                                          pbest, 
                                                                          gbest,
                                                                          gbest_id)
                obj = self.func(*particles)

                pbest_obj = np.array([pbest_obj, obj]).min(axis=0)
                pbest[:, (pbest_obj >= obj)] = particles[:, (pbest_obj >= obj)]

                gbest_id = pbest_obj.argmin()
                gbest = pbest[:, gbest_id]
                gbest_obj_prev = gbest_obj
                gbest_obj = pbest_obj.min()
                if np.abs(gbest_obj_prev - gbest_obj) < self.eps:
                    no_diff_count += 1
            i += 1
        
        return {'Function opt.': gbest_obj, 'Opt. position': gbest, 'Number of iterations': i}


optimizer = PSO(func=free_energy, 
                n_variables=10, 
                n_particles=10)
                # c_1=0.5,
                # c_2=0.5)

# results = optimizer.optimize()

# for k, v in results.items():
#     print(k, v)

g_best = []
# g_best_position = []
iterations = []

for _ in range(30):
    result = optimizer.optimize()
    g_best.append(result['Function opt.'])
    iterations.append(result['Number of iterations'])
    # g_best_position.append(result['Opt. position'])
    
    
print(sum(g_best) / len(g_best))
print(sum(iterations) / len(iterations))
# x = np.array(g_best_position).mean(axis=1)
# x = results['Opt. position']

# print(x[0] + 2 * x[1] + 2 * x[2] + x[5] + x[9],
#       x[3] + 2 * x[4] + x[5] + x[6],
#       x[2] + x[6] + x[7] + 2 * x[8] + x[9])
